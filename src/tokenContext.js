import { createContext, useContext, useEffect, useState } from 'react';

const TokenContext = createContext(null);

export const TokenProvider = ({ children }) => {
    const [token, setToken] = useState(localStorage.getItem('token'));
    const [user, setUser] = useState(null);

    useEffect(() => {
        const getUserInfo = async () => {
            try {
                const res = await fetch('http://localhost:4000/users', {
                    headers: {
                        Authorization: token,
                    },
                });
                const body = await res.json();

                setUser(body.data.infoUser);
            } catch (err) {
                console.error(err);
            }
        };

        if (token) getUserInfo();
    }, [token]);

    const setTokenInLocalStorage = (newToken) => {
        if (!newToken) {
            localStorage.removeItem('token');
            setUser(null);
        } else {
            localStorage.setItem('token', newToken);
        }
        setToken(newToken);
    };
    return (
        <TokenContext.Provider
            value={{ token, setToken: setTokenInLocalStorage, user, setUser }}
        >
            {children}
        </TokenContext.Provider>
    );
};

export const useToken = () => {
    return useContext(TokenContext);
};
