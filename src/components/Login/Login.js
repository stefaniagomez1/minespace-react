import { useState } from 'react';
import { Navigate } from 'react-router-dom';
import { useToken } from '../../tokenContext';
import './Login.css';

export const Login = () => {
    const [wrongData, setWrongData] = useState(false);

    const { token, setToken } = useToken();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [loading, setLoading] = useState(false);

    if (token) return <Navigate to="/dashboard" />;

    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);

        try {
            const res = await fetch('http://localhost:4000/login', {
                method: 'post',
                headers: {
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({
                    email,
                    password,
                }),
            });
            const body = await res.json();
            if (body.status === 'error') {
                setWrongData(true);
            } else {
                setToken(body.data.token);
            }
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
            setPassword('');
        }
    };
    return (
        <>
            <header>
                <h2>Login</h2>
            </header>
            <form className="loginForm" onSubmit={handleSubmit}>
                <label htmlFor="loginEmail">Email:</label>
                <input
                    type="email"
                    id="loginEmail"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <label htmlFor="loginPass">Password:</label>
                <input
                    type="password"
                    id="loginPass"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                <input disabled={loading} type="submit" value="Entrar" />
            </form>
            <section
                id="loginError"
                style={{ display: wrongData ? 'flex' : 'none' }}
            >
                <p id="loginErrorText">Usuario o contraseña incorrectos</p>
                <img src="/error.png" alt="error icon" />
            </section>
        </>
    );
};
