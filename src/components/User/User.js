import { UserInfo } from '../UserInfo/UserInfo';
import { UserNav } from '../UserNav/UserNav';

export const User = () => {
    return (
        <main>
            <UserNav />
            <UserInfo />
        </main>
    );
};
