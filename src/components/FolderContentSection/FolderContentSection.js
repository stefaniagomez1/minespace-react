import { useEffect, useState } from 'react';
import { useToken } from '../../tokenContext';
import './FolderContentSection.css';
import { Modal } from '../Modal/Modal';
import { Navigate, useLocation } from 'react-router-dom';

export const FolderContentSection = ({ showData, setShowData }) => {
    const { token } = useToken();

    const location = useLocation();

    const id = location?.state ? location.state.id : null;
    const [data, setData] = useState(null);
    // const [fileId, setFileId] = useState();
    const [deleteModal, setDeleteModal] = useState(null);

    useEffect(() => {
        try {
            const getData = async () => {
                const res = await fetch(`http://localhost:4000/folders/${id}`, {
                    headers: {
                        Authorization: token,
                    },
                });

                if (res.ok) {
                    const body = await res.json();
                    setData(body?.data?.folder);
                }
            };
            getData();
        } catch (err) {
            console.error(err);
        }
    }, [token, showData, id]);

    if (!token) return <Navigate to="/" />;

    const deleteFile = async (folderId, fileId) => {
        const res = await fetch(
            `http://localhost:4000/folders/${folderId}/files/${fileId}`,
            {
                method: 'delete',
                headers: {
                    Authorization: token,
                },
            }
        );
        if (res.ok) {
            const body = await res.json();
            setDeleteModal(null);
            setShowData(body);
        }
    };

    const downloadFile = async (fileName) => {
        const res = await fetch(`http://localhost:4000/download/${fileName}`, {
            headers: {
                Authorization: token,
            },
        });

        const blob = await res.blob();

        const url = window.URL.createObjectURL(new Blob([blob]));

        const link = document.createElement('a');

        link.href = url;

        link.download = fileName;

        link.click();
    };

    return (
        <section
            id="FolderContentSection"
            className={data?.length < 1 ? 'empty' : ''}
        >
            <ul>
                {data?.map((item) => {
                    return (
                        <li key={item.id} className="fileLi">
                            <article
                                onClick={() => {
                                    downloadFile(Object.values(item)[1]);
                                }}
                            >
                                <img
                                    className="icon"
                                    src="/fileIcon.svg"
                                    alt="file icon"
                                ></img>
                                <p>{Object.values(item)[1].slice(37)}</p>
                            </article>
                            <img
                                src="/papelera.svg"
                                alt="bin icon"
                                className="deleteIcon"
                                onClick={() => setDeleteModal(item.id)}
                            ></img>

                            <Modal
                                isOpen={deleteModal === item.id}
                                onClose={() => setDeleteModal(null)}
                            >
                                <p className="modalDelete">
                                    MineSpace va a eliminar{' '}
                                    {Object.values(item)[1].slice(37)} de manera
                                    permanente, ¿Desea continuar?
                                </p>
                                <div>
                                    <button
                                        onClick={() => {
                                            deleteFile(id, item.id);
                                        }}
                                    >
                                        Aceptar
                                    </button>
                                    <button
                                        onClick={() => {
                                            setDeleteModal(null);
                                        }}
                                    >
                                        Cancelar
                                    </button>
                                </div>
                            </Modal>
                        </li>
                    );
                })}
            </ul>
        </section>
    );
};
