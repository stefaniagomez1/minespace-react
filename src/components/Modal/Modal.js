import './Modal.css';

export const Modal = ({ isOpen, onClose, children }) => {
    return (
        <div className="modal" style={{ display: isOpen ? 'flex' : 'none' }}>
            <div className="modal-container">
                <button id="modal-close" onClick={onClose}>
                    X
                </button>
                {children}
            </div>
        </div>
    );
};
