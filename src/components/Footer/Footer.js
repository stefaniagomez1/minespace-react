import './Footer.css';

export const Footer = () => {
    return (
        <footer>
            <p className="footerP">
                MINESPACE 2023 <img src="/copyfly.png" alt="copyright" /> Creado
                por Estefania Gomez, Xurxo Sobrino y Alejandro Lopez
            </p>
        </footer>
    );
};
