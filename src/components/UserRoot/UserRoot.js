import './UserRoot.css';
import { UserRootNav } from '../UserRootNav/UserRootNav';
import { UserRootSection } from '../UserRootSection/UserRootSection';
import { Logout } from '../Logout/Logout';

export const UserRoot = ({ showData, setShowData }) => {
    return (
        <main>
            <Logout />
            <UserRootNav setShowData={setShowData} />
            <UserRootSection showData={showData} setShowData={setShowData} />
        </main>
    );
};
