import { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useToken } from '../../tokenContext';
import './FolderContentNav.css';

export const FolderContentNav = ({ setShowData }) => {
    const [file, setFile] = useState();
    const { token } = useToken();

    const location = useLocation();
    const navigate = useNavigate();

    const id = location?.state ? location.state.id : null;

    useEffect(() => {
        const uploadFiles = async () => {
            try {
                const formData = new FormData();

                formData.append('file', file);

                if (file) {
                    const res = await fetch(
                        `http://localhost:4000/folders/${id}/files`,
                        {
                            method: 'post',
                            headers: {
                                Authorization: token,
                            },
                            body: formData,
                        }
                    );
                    const body = await res.json();
                    setShowData(body);
                }
            } catch (err) {
                console.error(err);
            }
        };
        uploadFiles();
    }, [file, token, setShowData, id]);

    return (
        <nav>
            <form>
                <label htmlFor="uploadFile">
                    <img
                        className="uploadIcon"
                        src="/upload.png"
                        alt="Upload icon"
                    />
                </label>
                <input
                    type="file"
                    id="uploadFile"
                    className="hidden"
                    onChange={(e) => setFile(e.target.files[0])}
                />
            </form>
            <button
                id="returnHome"
                onClick={() => {
                    navigate('/dashboard');
                }}
            ></button>
        </nav>
    );
};
