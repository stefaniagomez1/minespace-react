import { useNavigate } from 'react-router-dom';
import './UserNav.css';

export const UserNav = () => {
    const navigate = useNavigate();

    return (
        <nav id="UserInfo">
            <button
                id="returnUserHome"
                onClick={() => {
                    navigate('/dashboard');
                }}
            ></button>
        </nav>
    );
};
