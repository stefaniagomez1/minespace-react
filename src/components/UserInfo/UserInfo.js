import { useToken } from '../../tokenContext';
import { useState } from 'react';
import './UserInfo.css';
import { Navigate } from 'react-router-dom';

export const UserInfo = () => {
    const { token, user, setUser } = useToken();

    const [loading, setLoading] = useState(false);

    const [username, setUsername] = useState(user?.name);
    const [usernameDisplay, setUsernameDisplay] = useState(false);
    const [email, setEmail] = useState(user?.email);
    const [emailDisplay, setEmailDisplay] = useState(false);
    const [currentPassword, setCurrentPassword] = useState();
    const [password, setPassword] = useState('');
    const [repeatedPassword, setRepeatedPassword] = useState('');
    const [passwordDisplay, setPasswordDisplay] = useState(false);
    const [passError, setPassError] = useState(false);

    if (!token) return <Navigate to="/" />;

    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);
        setPassError(false);

        try {
            const res = await fetch('http://localhost:4000/users', {
                method: 'PUT',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: username,
                    email,
                    password,
                    currentPassword,
                }),
            });
            const body = await res.json();

            if (body.status === 'error') {
                setPassError(body.message);
            }

            setUser({
                ...user,
                name: body.data?.user.name,
                email: body.data?.user.email,
            });
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
            if (currentPassword !== undefined) {
                setCurrentPassword('');
                setPassword('');
                setRepeatedPassword('');
            }
        }
    };

    return (
        <>
            <form onSubmit={handleSubmit} id="userInfoForm">
                <label htmlFor="username">Username:</label>
                <p style={{ display: usernameDisplay ? 'none' : 'block' }}>
                    {user?.name}
                </p>
                <img
                    src="/edit.svg"
                    alt="edit icon"
                    className="editInfo"
                    onClick={() => {
                        setUsernameDisplay(!usernameDisplay);
                    }}
                    style={{ display: usernameDisplay ? 'none' : 'block' }}
                ></img>
                <input
                    type="text"
                    onChange={(e) => setUsername(e.target.value)}
                    value={username}
                    id="username"
                    style={{ display: usernameDisplay ? 'block' : 'none' }}
                ></input>
                <img
                    src="/return.svg"
                    alt="return icon"
                    className="returnInfo"
                    onClick={() => {
                        setUsernameDisplay(!usernameDisplay);
                    }}
                    style={{ display: usernameDisplay ? 'block' : 'none' }}
                ></img>
                <label htmlFor="email">Email:</label>
                <p style={{ display: emailDisplay ? 'none' : 'block' }}>
                    {user?.email}
                </p>
                <img
                    src="/edit.svg"
                    alt="edit icon"
                    className="editInfo"
                    onClick={() => {
                        setEmailDisplay(!emailDisplay);
                    }}
                    style={{ display: emailDisplay ? 'none' : 'block' }}
                ></img>
                <input
                    type="email"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    id="email"
                    style={{ display: emailDisplay ? 'block' : 'none' }}
                ></input>
                <img
                    src="/return.svg"
                    alt="return icon"
                    className="returnInfo"
                    onClick={() => {
                        setEmailDisplay(!emailDisplay);
                    }}
                    style={{ display: emailDisplay ? 'block' : 'none' }}
                ></img>
                <label style={{ display: passwordDisplay ? 'none' : 'block' }}>
                    Password
                </label>
                <img
                    src="/edit.svg"
                    alt="edit icon"
                    className="editInfo"
                    onClick={() => {
                        setPasswordDisplay(!passwordDisplay);
                    }}
                    style={{ display: passwordDisplay ? 'none' : 'block' }}
                ></img>
                <label
                    htmlFor="currentPass"
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                >
                    Password Actual
                </label>
                <input
                    id="currentPass"
                    type="password"
                    onChange={(e) => setCurrentPassword(e.target.value)}
                    value={currentPassword}
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                ></input>
                <label
                    htmlFor="password"
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                >
                    Nueva Password
                </label>
                <input
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                    id="password"
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                ></input>
                <label
                    htmlFor="repeatedPass"
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                >
                    Repetir Password
                </label>
                <input
                    type="password"
                    onChange={(e) => setRepeatedPassword(e.target.value)}
                    value={repeatedPassword}
                    id="repeatedPass"
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                ></input>
                <img
                    src="/return.svg"
                    alt="return icon"
                    className="returnInfo"
                    onClick={() => {
                        setPasswordDisplay(!passwordDisplay);
                    }}
                    style={{ display: passwordDisplay ? 'block' : 'none' }}
                ></img>
                <p
                    className="UserInfoError"
                    style={{ display: passError ? 'block' : 'none' }}
                >
                    {passError}
                </p>
                <p
                    className="UserInfoError"
                    style={{
                        display:
                            password !== repeatedPassword ? 'block' : 'none',
                    }}
                >
                    "Nueva Password" y "Repetir Password" deben coincidir
                </p>
                <button
                    id="UserInfoSubmit"
                    disabled={password !== repeatedPassword || loading}
                >
                    Enviar
                </button>
            </form>
        </>
    );
};
