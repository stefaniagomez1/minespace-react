import { NavLink } from 'react-router-dom';
import './Header.css';

export const Header = () => {
    return (
        <header id="mainHeader">
            <NavLink to="/">
                <img src="/minespace_header.png" alt="minespace logo"></img>
            </NavLink>
            <h1>
                <NavLink to="/">MineSpace</NavLink>
            </h1>
        </header>
    );
};
