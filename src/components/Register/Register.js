import { useState } from 'react';
import './Register.css';

export const Register = ({ onClose, openLogin }) => {
    const [repeatedEmail, setRepeatedEmail] = useState(false);

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [repeatedPass, setRepeatedPass] = useState('');

    const [loading, setLoading] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);
        try {
            const res = await fetch('http://localhost:4000/users', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name,
                    email,
                    password,
                }),
            });
            const body = await res.json();
            if (body.status === 'error') {
                setRepeatedEmail(body.message);
            } else {
                onClose();
                openLogin(true);
            }
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
            setName('');
            setEmail('');
            setPassword('');
            setRepeatedPass('');
        }
    };

    return (
        <>
            <header>
                <h2>Register</h2>
            </header>
            <form className="registerForm" onSubmit={handleSubmit}>
                <label htmlFor="registerName">Nombre de usuario:</label>
                <input
                    type="text"
                    id="registerName"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    autoFocus
                    required
                />
                <label htmlFor="registerEmail">Email:</label>
                <input
                    type="email"
                    id="registerEmail"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <label htmlFor="registerPass">Password:</label>
                <input
                    type="password"
                    id="registerPass"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                <label htmlFor="repeatPass">Repeat password</label>
                <input
                    type="password"
                    id="repeatPass"
                    value={repeatedPass}
                    onChange={(e) => setRepeatedPass(e.target.value)}
                    required
                />
                <p
                    className="passwordRegisterError"
                    style={{
                        display: password === repeatedPass ? 'none' : 'flex',
                    }}
                >
                    Las contraseñas no coinciden
                </p>
                <input
                    type="submit"
                    value="Enviar"
                    disabled={loading || password !== repeatedPass}
                />
            </form>
            <section
                id="registerError"
                style={{ display: repeatedEmail ? 'flex' : 'none' }}
            >
                <img src="/error.png" alt="error icon" />
                <p className="registerErrorText">{repeatedEmail}</p>
            </section>
        </>
    );
};
