import { useState } from 'react';

import './Root.css';
import { Register } from '../Register/Register';
import { Login } from '../Login/Login';
import { Modal } from '../Modal/Modal';

export const Root = () => {
    const [mostrarRegistro, setMostrarRegistro] = useState(false);
    const [mostrarLogin, setMostrarLogin] = useState(false);

    return (
        <main id="Root">
            <p>Bienvenido a tu espacio infinito</p>
            <div>
                <button onClick={() => setMostrarRegistro(true)}>
                    Register
                </button>
                <Modal
                    isOpen={mostrarRegistro}
                    onClose={() => setMostrarRegistro(false)}
                >
                    <Register
                        onClose={() => setMostrarRegistro(false)}
                        openLogin={setMostrarLogin}
                    />
                </Modal>
                <button onClick={() => setMostrarLogin(true)}>Login</button>
                <Modal
                    isOpen={mostrarLogin}
                    onClose={() => setMostrarLogin(false)}
                >
                    <Login />
                </Modal>
            </div>
        </main>
    );
};
