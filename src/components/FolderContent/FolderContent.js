import { FolderContentNav } from '../FolderContentNav/FolderContentNav';
import { FolderContentSection } from '../FolderContentSection/FolderContentSection';
import { Logout } from '../Logout/Logout';

export const FolderContent = ({ showData, setShowData }) => {
    return (
        <main>
            <Logout />
            <FolderContentNav setShowData={setShowData} />
            <FolderContentSection
                showData={showData}
                setShowData={setShowData}
            />
        </main>
    );
};
