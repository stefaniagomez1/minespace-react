import { useState } from 'react';
import { useToken } from '../../tokenContext';
import './NewFolder.css';

export const NewFolder = ({ setShowData, onClose }) => {
    const [folderName, setFolderName] = useState('');
    const [loading, setLoading] = useState(false);
    const { token } = useToken();

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        try {
            const res = await fetch('http://localhost:4000/folders', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: token,
                },
                body: JSON.stringify({
                    name: folderName,
                }),
            });

            const body = await res.json();
            setShowData(body);
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
            setFolderName('');
            onClose();
        }
    };

    return (
        <form onSubmit={handleSubmit} id="NewFolder">
            <label htmlFor="newFolderName">Nombre nueva carpeta</label>
            <input
                type="text"
                id="newFolderName"
                onChange={(e) => setFolderName(e.target.value)}
                value={folderName}
            ></input>
            <input type="submit" disabled={loading} value="Crear" />
        </form>
    );
};
