import { useEffect, useState } from 'react';

import './UserRootNav.css';
import { useToken } from '../../tokenContext';
import { Modal } from '../Modal/Modal';
import { NewFolder } from '../NewFolder/NewFolder';

export const UserRootNav = ({ setShowData }) => {
    const [file, setFile] = useState();
    const { token } = useToken();
    const [showNewFolder, setShowNewFolder] = useState(false);

    useEffect(() => {
        const uploadFiles = async () => {
            try {
                const formData = new FormData();

                formData.append('file', file);

                if (file) {
                    const id = await fetch('http://localhost:4000/folders', {
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: token,
                        },
                    });

                    const bodyId = await id.json();

                    const res = await fetch(
                        `http://localhost:4000/folders/${bodyId.data.allData[0].staticFolderId[0].id}/files`,
                        {
                            method: 'post',
                            headers: {
                                Authorization: token,
                            },
                            body: formData,
                        }
                    );

                    const body = await res.json();
                    setShowData(body);
                }
            } catch (err) {
                console.error(err);
            }
        };
        uploadFiles();
    }, [file, token, setShowData]);

    return (
        <nav>
            <form id="UserRootNav">
                <input
                    type="file"
                    id="uploadFile"
                    className="hidden"
                    onChange={(e) => setFile(e.target.files[0])}
                />
                <label htmlFor="uploadFile">
                    <img
                        className="uploadIcon"
                        src="/upload.png"
                        alt="Upload icon"
                    />
                </label>
            </form>
            <button
                id="newFolder"
                onClick={() => {
                    setShowNewFolder(true);
                }}
            />
            <Modal
                isOpen={showNewFolder}
                onClose={() => setShowNewFolder(false)}
            >
                <NewFolder
                    setShowData={setShowData}
                    onClose={() => setShowNewFolder(false)}
                />
            </Modal>
        </nav>
    );
};
