import { useState, useEffect } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { useToken } from '../../tokenContext';
import './Logout.css';

export const Logout = () => {
    const { token, setToken } = useToken();
    const [loading, setLoading] = useState(false);

    const [userName, setUserName] = useState();

    const navigate = useNavigate();

    useEffect(() => {
        const getUserInfo = async () => {
            try {
                const res = await fetch('http://localhost:4000/users', {
                    headers: {
                        Authorization: token,
                    },
                });
                const body = await res.json();
                setUserName(body.data.infoUser.name);
            } catch (err) {
                console.error(err);
            }
        };

        getUserInfo();
    }, [token]);

    if (!token) return <Navigate to="/" />;

    return (
        <section id="logoutSection">
            <p onClick={() => navigate('/userInfo')} id="infoUser">
                @{userName}
            </p>

            <button
                id="logoutButton"
                onClick={() => {
                    setToken(null);
                    setLoading(!loading);
                }}
                disabled={loading}
            >
                Logout
            </button>
        </section>
    );
};
