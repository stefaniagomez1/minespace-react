import { Fragment, useEffect, useState } from 'react';
import { useToken } from '../../tokenContext';
import './UserRootSection.css';
import { Navigate, useNavigate } from 'react-router-dom';
import { Modal } from '../Modal/Modal';

export const UserRootSection = ({ showData, setShowData }) => {
    const { token } = useToken();
    const navigate = useNavigate();

    const [data, setData] = useState(null);
    const [deleteModal, setDeleteModal] = useState(null);

    useEffect(() => {
        try {
            const getData = async () => {
                const res = await fetch('http://localhost:4000/folders', {
                    headers: {
                        Authorization: token,
                    },
                });
                if (res.ok) {
                    const body = await res.json();

                    setData(body.data.allData);
                }
            };
            getData();
        } catch (err) {
            console.error(err);
        }
    }, [token, showData]);

    if (!token) return <Navigate to="/" />;

    const deleteFolder = async (folderId) => {
        const res = await fetch(`http://localhost:4000/folders/${folderId}`, {
            method: 'delete',
            headers: {
                Authorization: token,
            },
        });
        if (res.ok) {
            const body = await res.json();
            setDeleteModal(null);
            setShowData(body);
        }
    };

    const deleteFile = async (folderId, fileId) => {
        const res = await fetch(
            `http://localhost:4000/folders/${folderId}/files/${fileId}`,
            {
                method: 'delete',
                headers: {
                    Authorization: token,
                },
            }
        );
        if (res.ok) {
            const body = await res.json();
            setDeleteModal(null);
            setShowData(body);
        }
    };

    const downloadFile = async (fileName) => {
        const res = await fetch(`http://localhost:4000/download/${fileName}`, {
            headers: {
                Authorization: token,
            },
        });

        const blob = await res.blob();

        const url = window.URL.createObjectURL(new Blob([blob]));

        const link = document.createElement('a');

        link.href = url;

        link.download = fileName;

        link.click();
    };

    return (
        <section
            id="UserRootSection"
            className={data?.length < 2 ? 'empty' : ''}
        >
            <ul>
                {data?.map((item) => {
                    const id = item.folderId || item.fileId;

                    if (Object.keys(item)[0] === 'folderName') {
                        return (
                            <li key={id}>
                                <article
                                    className="folderArticle"
                                    onClick={() => {
                                        navigate(
                                            `/dashboard/${
                                                Object.values(item)[0]
                                            }`,
                                            {
                                                state: {
                                                    id: Object.values(item)[1],
                                                },
                                            }
                                        );
                                    }}
                                >
                                    <img
                                        className="icon"
                                        src="/folderIcon.png"
                                        alt="folder icon"
                                    ></img>
                                    <p>{Object.values(item)[0]}</p>
                                </article>
                                <img
                                    src="/papelera.svg"
                                    className="deleteIcon"
                                    alt="bin icon"
                                    onClick={() => setDeleteModal(id)}
                                ></img>

                                <Modal
                                    isOpen={deleteModal === id}
                                    onClose={() => setDeleteModal(null)}
                                >
                                    <p className="modalDelete">
                                        MineSpace va a eliminar{' '}
                                        {Object.values(item)[0]} de manera
                                        permanente, ¿Desea continuar?
                                    </p>
                                    <div>
                                        <button
                                            onClick={() => {
                                                deleteFolder(id);
                                            }}
                                        >
                                            Aceptar
                                        </button>
                                        <button
                                            onClick={() => {
                                                setDeleteModal(null);
                                            }}
                                        >
                                            Cancelar
                                        </button>
                                    </div>
                                </Modal>
                            </li>
                        );
                    } else if (Object.keys(item)[0] === 'fileName') {
                        return (
                            <li key={id}>
                                <article
                                    onClick={() => {
                                        downloadFile(Object.values(item)[0]);
                                    }}
                                >
                                    <img
                                        className="icon"
                                        src="/fileIcon.svg"
                                        alt="file icon"
                                    ></img>
                                    <p>{Object.values(item)[0].slice(37)}</p>
                                </article>
                                <img
                                    src="/papelera.svg"
                                    alt="bin icon"
                                    className="deleteIcon"
                                    onClick={() => setDeleteModal(id)}
                                ></img>

                                <Modal
                                    isOpen={deleteModal === id}
                                    onClose={() => setDeleteModal(null)}
                                >
                                    <p className="modalDelete">
                                        MineSpace va a eliminar{' '}
                                        {Object.values(item)[0].slice(37)} de
                                        manera permanente, ¿Desea continuar?
                                    </p>
                                    <div>
                                        <button
                                            onClick={() => {
                                                deleteFile(
                                                    Object.values(item)[2],
                                                    id
                                                );
                                            }}
                                        >
                                            Aceptar
                                        </button>
                                        <button
                                            onClick={() => {
                                                setDeleteModal(null);
                                            }}
                                        >
                                            Cancelar
                                        </button>
                                    </div>
                                </Modal>
                            </li>
                        );
                    } else {
                        return <Fragment key="staticId"></Fragment>;
                    }
                })}
            </ul>
        </section>
    );
};
