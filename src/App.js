import { Routes, Route } from 'react-router-dom';

import './App.css';
import { Header } from './components/Header/Header';
import { Root } from './components/Root/Root';
import { UserRoot } from './components/UserRoot/UserRoot';
import { FolderContent } from './components/FolderContent/FolderContent';
import { Footer } from './components/Footer/Footer';
import { useState } from 'react';
import { User } from './components/User/User';

function App() {
    const [showData, setShowData] = useState();

    return (
        <div className="App">
            <Header />
            <Routes>
                <Route path="/" element={<Root />} />
                <Route path="/userInfo" element={<User />} />
                <Route
                    path="/dashboard"
                    element={
                        <UserRoot
                            showData={showData}
                            setShowData={setShowData}
                        />
                    }
                />
                <Route
                    path="/dashboard/:folderName"
                    element={
                        <FolderContent
                            showData={showData}
                            setShowData={setShowData}
                        />
                    }
                />
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
