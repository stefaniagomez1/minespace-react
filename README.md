MineSpace API (Frontend)

API que permite a los usuarios registrarse y acceder a un espacio personal donde subir ficheros, creada por Alejandro López, Xurxo Sobrino y Estefanía Gómez.

Backend de esta API:
https://gitlab.com/Radikkal/minespace

Instalar

1️⃣ Ejecutar npm i para instalar dependencias.
2️⃣ Iniciar el servidor de Backend.
3️⃣ Ejecutar npm start para iniciar el cliente de React.
